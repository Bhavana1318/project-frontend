import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class EmpService {
  addToCart(product: any, quantity: number) {
    throw new Error('Method not implemented.');
  }
  
  private loginStatus : boolean;
  constructor(private httpClient : HttpClient,) {
    this.loginStatus = false;

   }
   API = 'http://localhost:8085';
   getLoginStatus(){
    return this.loginStatus;
  }

  setLoginStatus(){
    this.loginStatus = true;
  }
  setLogoutStatus(){
    this.loginStatus = false;
  }


  getCountries(){
    return this.httpClient.get('https://restcountries.com/v2/all');
  }
  
  // getEmployees(){ //Observables
  //   return this.httpClient.get(this.API+'getAllEmployees');
  // }
  // registerService(user: any){
  //   return this.httpClient.post('registerUser',user);
  //  }


  deleteUser(userId:number){
    return this.httpClient.delete(this.API+'/deleteUser/'+userId);
  }
  updateUser(edituser: any) {
    return this.httpClient.put(this.API+'/update', edituser);
   }
   getAllUsers(){
    return this.httpClient.get(this.API+'/users');
   }
   registerUser(user:any){
    return this.httpClient.post(this.API+'/registerUser',user);
   }


   deleteArtist(userId:number){
    return this.httpClient.delete(this.API+'/deleteArtist/'+userId);
  }
  updateArtist(edituser: any) {
    return this.httpClient.put(this.API+'/updateArtist', edituser);
   }
   getAllArtist(){
    return this.httpClient.get(this.API+'/artists');
   }
   registerArtist(user:any){
    return this.httpClient.post(this.API+'/registerArtist',user);
   }
  //  setUserLoggedIn(){
  //   this.isUserLogged = true;
  // }  

   uploadForm(form:any){
    return this.httpClient.post(this.API+'/uploadForm',form);
   }
   uploadPaintings(form:any){
    return this.httpClient.post(this.API+'/uploadPaintings',form);
   }
   getAllPaintings(){
    return this.httpClient.get(this.API+'/getAllPaintings');
   }
   getAllApprove(){
    return this.httpClient.get(this.API+'/getAllApprove');
   }

     getCartItems(): any[] {
       // Implement the logic to retrieve cart items
       // and return them as an array
       return []; // Replace this with your actual implementation
     }
     getGrandTotal(): number {
       // Implement the logic to calculate and return the grand total
       return 0; // Replace this with your actual implementation
     }
     removeFromCart(item: any): void {
       // Implement the logic to remove the item from the cart
     }
     clearCart(): void {
       // Implement the logic to clear the cart
     }
   }
   





