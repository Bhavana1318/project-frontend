import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-santhosh',
  templateUrl: './santhosh.component.html',
  styleUrls: ['./santhosh.component.css']
})
export class SanthoshComponent implements OnInit {

  gallery: any;
  cartItems: any;

  constructor() {


    this.gallery = [
      {id: 1001,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img12.jpg'},
      {id: 1002,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img13.jpg'},
      {id: 1003,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img14.jpg'},
      {id: 1004,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img15.jpg'},
      {id: 1005,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img16.jpg'},
      {id: 1006,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img17.jpg'},
      {id: 1007,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img18.jpg'},
      {id: 1008,name:"By santhosh",description:"Landscape",size:"16*16", price:"Rs  3499.00", imagePath:'assets/santhosh/img19.jpg'},
      
      ];
  }

  ngOnInit(): void {
  }
  addToCart(item: any){
    console.log(item)
  }
}