import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-watercolor',
  templateUrl: './watercolor.component.html',
  styleUrls: ['./watercolor.component.css']
})
export class WatercolorComponent implements OnInit {

  watercolor: any;
  cartItems: any;

  constructor() {


    this.watercolor= [
      {id: 1001, price:3499.00, imagePath:'assets/images3/img37.jpg'},
      {id: 1002, price:3499.00, imagePath:'assets/images3/img38.jpg'},
      {id: 1003, price:3499.00, imagePath:'assets/images3/img39.jpg'},
      {id: 1004, price:3499.00, imagePath:'assets/images3/img40.jpg'},
      {id: 1005, price:3499.00, imagePath:'assets/images3/img41.jpg'},
      {id: 1006, price:3499.00, imagePath:'assets/images3/img42.jpg'},
      {id: 1007, price:3499.00, imagePath:'assets/images3/img43.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images3/img44.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images3/img45.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images3/img46.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images3/img47.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images3/img48.jpg'}, ];
  }

  ngOnInit(): void {
  }
  addToCart(item: any){
    console.log(item)
  }
}