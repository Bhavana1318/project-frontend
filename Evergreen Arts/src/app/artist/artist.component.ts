import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {
  artist:any;
  cartitems:any;
  constructor(private service: EmpService){
    this.artist=[{id:101,name:'Santhosh',imagePath:'assets/santhosh/img3.jpg'},
                 {id:101,name:'Akash',imagePath:'assets/akash/img2.jpg'},
                 {id:101,name:'Bharath',imagePath:'assets/images/img9.jpg'},
                 {id:101,name:'Bharath',imagePath:'assets/images/img8.jpg'},
                 {id:101,name:'Bharath',imagePath:'assets/images/img12.jpg'},
                 {id:101,name:'Bharath',imagePath:'assets/images/img12.jpg'},
  ]
  this.service.getAllUsers().subscribe((data) => {
    this.artist = data;
  });
  };
  ngOnInit(): void {
  }
  addToCart(item: any){
    console.log(item)
  }
}