import { Component } from '@angular/core';
import { cartItem } from '../CartItem';
import { Router } from '@angular/router';
import { Product } from '../Product';
import { AddToCartService } from '../add-to-cart.service';

@Component({
  selector: 'app-addtocart',
  templateUrl: './addtocart.component.html',
  styleUrls: ['./addtocart.component.css']
})
export class AddtocartComponent {


    items: cartItem[] = [];
    quantity: number = 1;
  
    constructor(private service: AddToCartService, private router: Router) { }
  
    ngOnInit() {
      this.items = this.service.getItems();
      console.log(this.items);
    }
  
    addToCart(item: Product, quantity: number) {
      this.service.addToCart(item, quantity);
      console.log(item);
    }
  
    clearCart() {
      this.items = this.service.clearCart();
    }
  
    TotalBillAmount(): number {
      let total = 0;
      for (const item of this.items) {
        total += item.quantity * item.product.price;
        
      }
      return total;
    }
  
    payment(){
      this.router.navigate(['/payment']);
    }
    pay(){
      alert('please login to pay')
      this.router.navigate(['login'])
    }
    user() {
      return localStorage.getItem('user');
    }
    admin() {
      return localStorage.getItem('admin');
    }
    artist() {
      return localStorage.getItem('artist');
    }
    logout() {
      localStorage.clear();
    }
  }
