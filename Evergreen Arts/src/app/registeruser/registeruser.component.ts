import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { EncryDecryService } from '../encrydecry.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registeruser',
  templateUrl: './registeruser.component.html',
  styleUrls: ['./registeruser.component.css']
})
export class RegisteruserComponent {
  user: any;
  EncrDecr: any;
  
  constructor(private service: EmpService,private route: Router,private EncrypDecryp: EncryDecryService ){
    this.user={id:"",firstName:"",lastName:"",email:"",password:""};
  }

  registerUser(user: any) {
    var encrypted = this.EncrypDecryp.set('123456$#@$^@1ERF', user.password);
    user.password=encrypted;
    console.log(user);
    this.user = user;
    // console.log(user);
    this.service.registerUser(user).subscribe();
    this.route.navigate(['login']);
  }
  ngOnInit(): void {
    // throw new Error('Method not implemented.');
  } 

}
