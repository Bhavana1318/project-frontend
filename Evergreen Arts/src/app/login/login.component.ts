import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';
import { EncryDecryService } from '../encrydecry.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  users: any;
  loginId:string;
  password: string;
  artists:any;
  match:any;
  constructor(private service : EmpService, private router: Router,private EncrDecr: EncryDecryService ){
    this.loginId = '';
    this.password = '';
    this. match = false;

//     this.users=[{"empId":1001,"empName":"PASHA MD","salary":9999.99,"gender":"M","doj":"01-02-2011","emailId":"pasha@gmail.com","country":"INDIA","password":"PASSWORD"},
//   {"empId":1002,"empName":"HARSHA SREE","salary":88888.88,"gender":"M","doj":"04-08-2009","emailId":"harsha@gmail.com","country":"INDIA","password":"PASSWORD"},
//   {"empId":1003,"empName":"INDIRA","salary":7777.77,"gender":"F","doj":"03-09-2017","emailId":"indira@gmail.com","country":"INDIA","password":"PASSWORD"},
//   {"empId":1004,"empName":"PRUTHVI","salary":66666.66,"gender":"F","doj":"08-02-2013","emailId":"pruthvi@gmail.com","country":"INDIA","password":"PASSWORD"},
//   {"empId":1005,"empName":"ROBIN","salary":55555.55,"gender":"M","doj":"6-01-2012","emailId":"robin@gmail.com","country":"INDIA","password":"PASSWORD"}
// ];
this.service.getAllUsers().subscribe((data)=>{
  this.users = data;
  console.log(data)
})
this.service.getAllArtist().subscribe((data)=>{
  this.artists = data;
  console.log(data)
})
}

  // loginSubmit(){
  //   if(this.loginId == 'HR' && this.password=='HR'){

  //     alert('WELCOME TO HR HOME PAGE')
  //   }else{
  //     this.users.forEach((user:any) => {
  //       if(this.loginId == user.emailId && this.password == user.password){
  //         alert('Welcome to user Home Page');
  //       }
  //     });
  //   }
  // }

  submitLogin(loginForm: any) {
   
  
    if (loginForm.login == 'ADMIN' && loginForm.password == 'ADMIN') {
     this.match = true;
      this.service.setLoginStatus();
      localStorage.setItem('admin', 'HR');
      alert('WELCOME TO ADMIN HOME PAGE');
      this.router.navigate(['admin']);
    } 
    else {
      this.users.forEach((user: any) => {
        if (
          loginForm.login == user.email &&
          loginForm.password == this.EncrDecr.get('123456$#@$^@1ERF', user.password)
        ) {
         this.match = true;
          this.service.setLoginStatus();
          localStorage.setItem('user', JSON.stringify(user));
          alert('Welcome to user Home Page');
          this.router.navigate(['paintings']);
        }
      });
  

      if (!this.match) {
        this.artists.forEach((artist: any) => {
          if (
            loginForm.login == artist.email &&
            loginForm.password == this.EncrDecr.get('123456$#@$^@1ERF', artist.password)
          ) {
            this.match = true;
            this.service.setLoginStatus();
            localStorage.setItem('artist', JSON.stringify(artist));
            alert('Welcome to artist Home Page');
            this.router.navigate(['upload']);
          }
        });
      }
    }
  
    if (!this.match) {
      alert('Invalid email and password');
    }
  }
  
}
