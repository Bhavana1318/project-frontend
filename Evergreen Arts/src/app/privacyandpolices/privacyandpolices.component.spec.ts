import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyandpolicesComponent } from './privacyandpolices.component';

describe('PrivacyandpolicesComponent', () => {
  let component: PrivacyandpolicesComponent;
  let fixture: ComponentFixture<PrivacyandpolicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivacyandpolicesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrivacyandpolicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
