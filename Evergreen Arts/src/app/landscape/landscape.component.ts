import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landscape',
  templateUrl: './landscape.component.html',
  styleUrls: ['./landscape.component.css']
})
export class LandscapeComponent implements OnInit {

  landscape: any;
  cartItems: any;

  constructor() {


    this.landscape= [
      {id: 1001, price:3499.00, imagePath:'assets/images2/img25.jpg'},
      {id: 1002, price:3499.00, imagePath:'assets/images2/img26.jpg'},
      {id: 1003, price:3499.00, imagePath:'assets/images2/img27.jpg'},
      {id: 1004, price:3499.00, imagePath:'assets/images2/img28.jpg'},
      {id: 1005, price:3499.00, imagePath:'assets/images2/img29.jpg'},
      {id: 1006, price:3499.00, imagePath:'assets/images2/img30.jpg'},
      {id: 1007, price:3499.00, imagePath:'assets/images2/img31.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images2/img32.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images2/img33.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images2/img34.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images2/img35.jpg'},
      {id: 1008, price:3499.00, imagePath:'assets/images2/img36.jpg'}, ];
  }

  ngOnInit(): void {
  }
  addToCart(item: any){
    console.log(item)
  }
}