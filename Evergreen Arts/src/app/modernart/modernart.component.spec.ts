import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModernartComponent } from './modernart.component';

describe('ModernartComponent', () => {
  let component: ModernartComponent;
  let fixture: ComponentFixture<ModernartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModernartComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModernartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
