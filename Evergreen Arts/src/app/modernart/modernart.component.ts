import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modernart',
  templateUrl: './modernart.component.html',
  styleUrls: ['./modernart.component.css']
})
export class ModernartComponent implements OnInit {

  gallery: any;
  cartItems: any;

  constructor() {


    this.gallery = [
      {id: 1009,name:"By Bharath",description:"Modern art",size:"16*16", price:"Rs  3499.00", imagePath:'assets/modernart/img7.jpg'},
      {id: 1010,name:"By Bharath",description:"Modern art",size:"16*16", price:"Rs  3499.00", imagePath:'assets/modernart/img8.jpg'},
      {id: 1011,name:"By Bharath",description:"Modern art",size:"16*16", price:"Rs  3499.00", imagePath:'assets/modernart/img9.jpg'},
      {id: 1012,name:"By Bharath",description:"Modern art",size:"16*16", price:"Rs  3499.00", imagePath:'assets/modernart/img10.jpg'},
      {id: 1013,name:"By Bharath",description:"Modern art",size:"16*16", price:"Rs  3499.00", imagePath:'assets/modernart/img11.jpg'},
      
      
      
      ];
  }

  ngOnInit(): void {
  }
  addToCart(item: any){
    console.log(item)
  }

}