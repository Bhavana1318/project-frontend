import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { HomepageComponent } from './homepage/homepage.component';
import { CollectionComponent } from './collection/collection.component';
import { GalleryservicesComponent } from './galleryservices/galleryservices.component';
import { PaintingsComponent } from './paintings/paintings.component';
import { ContactusComponent } from './contactus/contactus.component';
import { RegisterComponent } from './register/register.component';
import { ArtistComponent } from './artist/artist.component';
import { UploadComponent } from './upload/upload.component';
import { HRComponent } from './hr/hr.component';
import { LandscapeComponent } from './landscape/landscape.component';
import { PrivacyandpolicesComponent } from './privacyandpolices/privacyandpolices.component';
import { WatercolorComponent } from './watercolor/watercolor.component';
import { SanthoshComponent } from './santhosh/santhosh.component';
import { PaymentComponent } from './payment/payment.component';
import { VillageComponent } from './village/village.component';
import { ModernartComponent } from './modernart/modernart.component';
import { TermsandcComponent } from './termsandc/termsandc.component';
import { AuthbuttonComponent } from './authbutton/authbutton.component';
import { AddtocartComponent } from './addtocart/addtocart.component';
import { RegisterartistComponent } from './registerartist/registerartist.component';
import { RegisteruserComponent } from './registeruser/registeruser.component';
import { AkashComponent } from './akash/akash.component';
import { WriteforusComponent } from './writeforus/writeforus.component';



const routes: Routes = [{path:'',component:HomepageComponent},
                         {path:'homepage', component:HomepageComponent},
                         {path:'artist', component:ArtistComponent},
                         {path:'login', component:LoginComponent},
                         {path:'register', component:RegisterComponent},
                         {path:'paintings', component:PaintingsComponent},
                         {path:'galleryservices', component:GalleryservicesComponent},
                         {path:'aboutus', component:AboutusComponent},
                         {path:'contactus', component:ContactusComponent},
                         {path: 'upload', component:UploadComponent },
                         {path:'collection', component:CollectionComponent},
                         {path:'landscape', component:LandscapeComponent},
                         {path:'watercolor', component:WatercolorComponent},
                         {path:'santhosh', component:SanthoshComponent},
                         {path:'village', component:VillageComponent},
                         {path:'modernart', component:ModernartComponent},
                         {path:'termsandc', component:TermsandcComponent},
                         {path:'payment', component:PaymentComponent},
                         {path:'privacyandpolices', component:PrivacyandpolicesComponent},
                         {path:'admin', component: HRComponent},
                         {path:'addtocart', component:AddtocartComponent},
                         {path:'authbutton',component:AuthbuttonComponent},
                         {path:'registerartist',component:RegisterartistComponent},
                         {path:'registeruser',component:RegisteruserComponent},
                         {path:'akash', component:AkashComponent},
                         {path:'writeforus', component:WriteforusComponent}
                      ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


// canActivate:[AuthenticGuard],
