import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { HomepageComponent } from './homepage/homepage.component';
import { FooterComponent } from './footer/footer.component';
import { CollectionComponent } from './collection/collection.component';
import { GalleryservicesComponent } from './galleryservices/galleryservices.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PaintingsComponent } from './paintings/paintings.component';
import { HRComponent } from './hr/hr.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ArtistComponent } from './artist/artist.component';
import { UploadComponent } from './upload/upload.component';
import { AkashComponent } from './akash/akash.component';
import { LandscapeComponent } from './landscape/landscape.component';
import { PrivacyandpolicesComponent } from './privacyandpolices/privacyandpolices.component';
import { SanthoshComponent } from './santhosh/santhosh.component';
import { WatercolorComponent } from './watercolor/watercolor.component';
import { TermsandcComponent } from './termsandc/termsandc.component';
import { PaymentComponent } from './payment/payment.component';
import { VillageComponent } from './village/village.component';
import { ModernartComponent } from './modernart/modernart.component';
import { AuthbuttonComponent } from './authbutton/authbutton.component';
import { AddtocartComponent } from './addtocart/addtocart.component';
import { RegisteruserComponent } from './registeruser/registeruser.component';
import { RegisterartistComponent } from './registerartist/registerartist.component';
import { WriteforusComponent } from './writeforus/writeforus.component';





@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    AboutusComponent,
    HomepageComponent,
    FooterComponent,
    CollectionComponent,
    GalleryservicesComponent,
    ContactusComponent,
    PaintingsComponent,
    HRComponent,
    RegisterComponent,
    ArtistComponent,
    UploadComponent,
    AkashComponent,
    LandscapeComponent,
    PrivacyandpolicesComponent,
    SanthoshComponent,
    WatercolorComponent,
    TermsandcComponent,
    PaymentComponent,
    VillageComponent,
    ModernartComponent,
    AuthbuttonComponent,
    AddtocartComponent,
    RegisteruserComponent,
    RegisterartistComponent,
    WriteforusComponent
  
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
