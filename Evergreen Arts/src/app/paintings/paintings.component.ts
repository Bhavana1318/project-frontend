import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { AddToCartService } from '../add-to-cart.service';

@Component({
  selector: 'app-paintings',
  templateUrl: './paintings.component.html',
  styleUrls: ['./paintings.component.css']
})
export class PaintingsComponent implements OnInit {
  datapaintings: any;
  painting: any;
  cartItems: any[] = [];


  constructor(private addservice: AddToCartService, private service : EmpService) {
    this.datapaintings = [
      { id: 1001, artistName: "By santhosh", artType: "Landscape", dimensions: "16*16", price: "Rs  3499.00", imagePath: 'assets/santhosh/img12.jpg' },
      // Rest of the paintings data...
    ];

    this.service.getAllPaintings().subscribe((data) => {
      this.datapaintings = data;
    });
  }

  ngOnInit(): void {
  }

  addToCart(product: any, quantity: number) {
    this.addservice.addToCart(product, quantity);
    window.alert('Product added to cart');
  }

  cart() {
    console.log('Button clicked');
  }
}
