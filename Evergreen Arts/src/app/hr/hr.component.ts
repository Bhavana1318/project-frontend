import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


declare var jQuery:any;
 

@Component({
  selector: 'app-hr',
  templateUrl: './hr.component.html',
  styleUrls: ['./hr.component.css']
})
export class HRComponent {
  editObject:any;
  people:any;
  artist:any;
  approvepaintings: any;
  submitted = false;
  email: String = '';

  constructor(private service: EmpService,private router: Router, private http:HttpClient){
    this.editObject={id:'',firstName:'',lastName:'',email:'',userType:''};
    this.service.getAllUsers().subscribe((data)=>{
      this.people = data;
    });
    this.service.getAllArtist().subscribe((data)=>{
      this.artist = data;
    });
    this.service.getAllApprove().subscribe((data) => {
      this.approvepaintings = data;
    });
  }
  editUser(user: any) {
    this.editObject = user;
    console.log('Opening modal...');
    jQuery('#userModel').modal('show');
    }
    userUpdate(){
      this.service.updateUser(this.editObject).subscribe();
        alert('successfully Updated......');
      }
      deleteUser(user:any){
        this.service.deleteUser(user.id).subscribe();
        alert('successFully deleted the User ' + user.id);
      }
      registerApprove(painting:any){
        this.service.uploadPaintings(painting).subscribe();
        this.http.post('/api/register', { email: this.email }).subscribe(() => {
          this.submitted = true;
          console.log('Email sent successfully');
        }, (error) => {
          console.error('Error sending email:', error);
        });

      }
}
