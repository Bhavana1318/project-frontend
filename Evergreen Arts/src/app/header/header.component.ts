import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(private service: EmpService, private router: Router) {
  }
  user() {
    return localStorage.getItem('user');
  }
  admin() {
    return localStorage.getItem('admin');
  }
  artist() {
    return localStorage.getItem('artist');
  }
  logout() {
    localStorage.clear();
  }
}


