import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent {
     constructor(private service: EmpService) {}
     
    
     uploadForm(form:any){
      this.service.uploadForm(form).subscribe();
      alert('your artwork is uploaded');
     }
   
  
}
