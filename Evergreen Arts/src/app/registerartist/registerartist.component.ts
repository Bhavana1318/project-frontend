import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';
import { EncryDecryService } from '../encrydecry.service';

@Component({
  selector: 'app-registerartist',
  templateUrl: './registerartist.component.html',
  styleUrls: ['./registerartist.component.css']
})
export class RegisterartistComponent {
user:any;
EncrDecr: any;

  constructor(private service: EmpService,private route: Router,private EncrypDecryp: EncryDecryService ){
    this.user={id:"",firstName:"",lastName:"",email:"",password:""};
  }

  registerArtist(user: any) {
      var encrypted = this.EncrypDecryp.set('123456$#@$^@1ERF', user.password);
      user.password=encrypted;
      console.log(user);
      this.user = user;
      // console.log(user);
      this.service.registerArtist(user).subscribe();
      this.route.navigate(['login']);
}
}
