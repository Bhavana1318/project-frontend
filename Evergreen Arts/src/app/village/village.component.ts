import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-village',
  templateUrl: './village.component.html',
  styleUrls: ['./village.component.css']
})
export class VillageComponent  implements OnInit {

  gallery: any;
  cartItems: any;

  constructor() {


    this.gallery = [
      {id: 1009,name:"By Harini",description:"Village Paintings",size:"16*16", price:"Rs  3499.00", imagePath:'assets/village/img20.jpg'},
      {id: 1010,name:"By Harini",description:"Village Paintings",size:"16*16", price:"Rs  3499.00", imagePath:'assets/village/img21.jpg'},
      {id: 1011,name:"By Harini",description:"Village Paintings",size:"16*16", price:"Rs  3499.00", imagePath:'assets/village/img22.jpg'},
      {id: 1012,name:"By Harini",description:"Village Paintings",size:"16*16", price:"Rs  3499.00", imagePath:'assets/village/img23.jpg'},
      {id: 1013,name:"By Harini",description:"Village Paintings",size:"16*16", price:"Rs  3499.00", imagePath:'assets/village/img24.jpg'},
      {id: 1014,name:"By Harini",description:"Village Paintings",size:"16*16", price:"Rs  3499.00", imagePath:'assets/village/img25.jpg'},
      
      
      ];
  }

  ngOnInit(): void {
  }
  addToCart(item: any){
    console.log(item)
  }
}