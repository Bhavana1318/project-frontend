import { Injectable } from '@angular/core';
import { Product } from './Product';
import { cartItem } from './CartItem';

@Injectable({
  providedIn: 'root'
})
export class AddToCartService {

  cart: cartItem[] = [];
  

  constructor() { }

  addToCart(product: Product,quantity : number) {
    let item: cartItem = {
      product, quantity,
      price: 0
    };
    this.cart.push(item);
  }

  getItems() {
    return this.cart;
  }

  clearCart() {
    this.cart = [];
    return this.cart;
  }

  TotalBillAmount(): number {
    let total = 0;
    for (const item of this.cart) {
      total += item.quantity * item.product.price;
    }
    return total;
  }
}