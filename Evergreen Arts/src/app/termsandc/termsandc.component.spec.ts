import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsandcComponent } from './termsandc.component';

describe('TermsandcComponent', () => {
  let component: TermsandcComponent;
  let fixture: ComponentFixture<TermsandcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TermsandcComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TermsandcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
