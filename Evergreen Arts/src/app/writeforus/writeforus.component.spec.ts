import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteforusComponent } from './writeforus.component';

describe('WriteforusComponent', () => {
  let component: WriteforusComponent;
  let fixture: ComponentFixture<WriteforusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WriteforusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WriteforusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
