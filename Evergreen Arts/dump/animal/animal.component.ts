import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './akash.component.html',
  styleUrls: ['./akash.component.css']
})
export class AkashComponent  implements OnInit {

  gallery: any;
  cartItems: any;


  
  constructor() {


    this.gallery = [
      {id: 1009,name:"By Akash",description:"BlackandWhite",size:"16*16", price:"Rs  3499.00", imagePath:'assets/akash/img1.jpg'},
      {id: 1010,name:"By Akash",description:"BlackandWhite",size:"16*16", price:"Rs  3499.00", imagePath:'assets/akash/img2.jpg'},
      {id: 1011,name:"By Akash",description:"BlackandWhite",size:"16*16", price:"Rs  3499.00", imagePath:'assets/akash/img3.jpg'},
      {id: 1012,name:"By Akash",description:"BlackandWhite",size:"16*16", price:"Rs  3499.00", imagePath:'assets/akash/img4.jpg'},
      {id: 1013,name:"By Akash",description:"BlackandWhite",size:"16*16", price:"Rs  3499.00", imagePath:'assets/akash/img5.jpg'},
      {id: 1014,name:"By Akash",description:"BlackandWhite",size:"16*16", price:"Rs  3499.00", imagePath:'assets/akash/img6.jpg'},
      
      
      ];
  }

  ngOnInit(): void {
  }
  addToCart(item: any){
    console.log(item)
  }
}